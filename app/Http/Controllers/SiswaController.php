<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function index(){
        $data = Siswa::all();
       return view('datasiswa',compact('data'));
    }

    public function tambahsiswa(){
        return view('tambahdata');
    }

    public function insertdata(Request $request){
        //dd($request->all());
        Siswa::create($request->all());
        return redirect()->route('pendaftaransiswabaru')->with('success','Data Berhasil Ditambahkan');
    }

    public function tampilkandata($id){

        $data = Siswa::find($id);
        //dd($data);

        return view('tampildata',compact('data'));
    }

    public function updatedata(Request $request, $id){
        $data = Siswa::find($id);
        $data->update($request->all());
        return redirect()->route('pendaftaransiswabaru')->with('success','Data Berhasil Ditambahkan');
    }

    public function delete($id){
        $data = Siswa::find($id);
        $data->delete();
        return redirect()->route('pendaftaransiswabaru')->with('success','Data Berhasil Ditambahkan');
    }
}
