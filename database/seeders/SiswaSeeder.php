<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswas')->insert([
            'nama' => 'munadi',
            'jeniskelamin' => 'cowo',
            'notelpon' => '087727657904',
            'alamat' => 'camplong',
            'jurusan' => 'mm',
        ]);
    }
}
